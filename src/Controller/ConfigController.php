<?php

namespace Drupal\media_keepeekdam\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns configs values.
 */
class ConfigController extends ControllerBase {


  const PAGES_TITLES = [
    'config' => 'configuration',
    'search' => 'import',
    'get_folder_child' => 'child folder',
    'save_html_cache' => 'save cache HTML folder',
    'get_html_cache' => 'get cache HTML folder',
    'asset_preview' => 'asset preview',
  ];

  /**
   * A config object to retrieve Keepeek DAM auth information from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * ClientFactory constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config object to retrieve Keepeek DAM config from.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('media_keepeekdam.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * A title callback method for test routes.
   *
   * @param array $_title_arguments
   *   Optional array from the route defaults.
   * @param string $_title
   *   Optional _title string from the route defaults.
   *
   * @return string
   *   The route title.
   */
  public function pageTitleCallback(array $_title_arguments = [], $_title = '') {
    if (isset(self::PAGES_TITLES[$_title_arguments['page']])) {
      $suffix = self::PAGES_TITLES[$_title_arguments['page']];

      return $this->config->get('global_name') . ' ' . $suffix;
    }
  }

}
