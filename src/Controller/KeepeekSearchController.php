<?php

namespace Drupal\media_keepeekdam\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Url;
use Drupal\media_keepeekdam\Plugin\media\Source\KeepeekMedia;
use Drupal\media_keepeekdam\Service\SearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\media_keepeekdam\Service\FolderTreeService;

/**
 * Returns responses for media_keepeekdam routes.
 */
class KeepeekSearchController extends ControllerBase {

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Keepeek search service.
   *
   * @var \Drupal\media_keepeekdam\Service\SearchService
   */
  protected $searchService;

  /**
   * Keepeek folder service.
   *
   * @var \Drupal\media_keepeekdam\Service\FolderTreeService
   */
  protected $folderTreeService;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The cache factory service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('form_builder'),
      $container->get('media_keepeekdam.search'),
      $container->get('request_stack'),
      $container->get('cache.bootstrap'),
      $container->get('media_keepeekdam.folder_tree')
    );
  }

  /**
   * Constructs a DbLogController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Drupal\media_keepeekdam\Service\SearchService $searchService
   *   The search service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache factory.
   * @param \Drupal\media_keepeekdam\Service\FolderTreeService $folderTreeService
   *   The folder tree service.
   */
  public function __construct(DateFormatterInterface $date_formatter, FormBuilderInterface $form_builder, SearchService $searchService, RequestStack $request_stack, CacheBackendInterface $cache, FolderTreeService $folderTreeService) {
    $this->dateFormatter = $date_formatter;
    $this->formBuilder = $form_builder;
    $this->searchService = $searchService;
    $this->requestStack = $request_stack;
    $this->cache = $cache;
    $this->folderTreeService = $folderTreeService;
  }

  /**
   * Displays a listing of database log messages.
   *
   * Messages are truncated at 56 chars.
   * Full-length messages can be viewed on the message details page.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   *
   * @see Drupal\dblog\Form\DblogClearLogConfirmForm
   * @see Drupal\dblog\Controller\DbLogController::eventDetails()
   */
  public function overview(Request $request) {

    if ($this->searchService->checkConfiguration()) {
      // Load parent folder.
      $folderTree = $this->folderTreeService->getTreeFolder('parent');
      $htmlCache = $this->getCacheHtml();
      if (!empty($htmlCache)) {
        $theme = 'keepeek_folder_cache';
        $folders = $htmlCache;
      }
      else {
        $theme = 'keepeek_folder_tree';
        $folders = $folderTree['_embedded']['child'];
      }

      // Render folderTree.
      $build['folderTree'] = [
        '#theme' => $theme,
        '#folders' => $folders,
        '#attached' => [
          'library' => [
            'media_keepeekdam/folder_tree',
          ],
        ],
      ];
      $build['keepeek_media_search_form'] = $this->formBuilder->getForm('Drupal\media_keepeekdam\Form\KeepeekSearchForm');
      $build['keepeek_table_select_form'] = $this->formBuilder->getForm('Drupal\media_keepeekdam\Form\KeepeekTableSelectForm');
    }
    else {

      $build['message'] = [
        '#markup' => '<p>' . $this->t('Media Keepeek DAM module is not yet configured.') . '</p>',
      ];
      $build['link'] = [
        '#type' => 'link',
        '#title' => 'Click here to configure it',
        '#url' => Url::fromRoute('media_keepeekdam.config', ['destination' => $this->requestStack->getCurrentRequest()->getRequestUri()]),
      ];
    }

    return $build;
  }

  /**
   * Load child folder by ID.
   *
   * @params int $id_folder
   *    Id For parent folder
   *
   * @return mixed
   *   A render html.
   */
  public function ajaxFolderChild($id_folder) {
    // Load child folder by ID.
    $folder_child = $this->folderTreeService->getTreeFolder('child', $id_folder);
    // Check the Keys of array 'folder_child' is numeric or not .
    $key_array = array_keys($folder_child['_embedded']['child']);
    // Check if a single element in array or several.
    $single_element = array_reduce($key_array, function ($c, $v) {
      return $c & (int) is_numeric($v);
    }, 1);
    $build = [
      '#theme' => 'keepeek_folder_child',
      '#childs' => $folder_child['_embedded']['child'],
      '#single_element'  => $single_element,
    ];
    return new Response(\Drupal::service('renderer')->render($build));
  }

  /**
   * Save Structure of folder.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   */
  public function saveCacheHtml(Request $request) {
    $content = $request->get('content');
    $this->requestStack
      ->getCurrentRequest()
      ->getSession()
      ->set('keepeek_structure_folder', $content);
    return new Response('Saved');
  }

  /**
   * Get Structure of folder from cache.
   */
  public function getCacheHtml() {
    return $this->requestStack
      ->getCurrentRequest()
      ->getSession()
      ->get('keepeek_structure_folder');
  }

  /**
   * Show preview asset by ID.
   *
   * @params int $id
   *    Keepeek ID.
   * @params string $mediaType
   *    Media type.
   *
   * @return mixed
   *   The preview of Keepeek asset.
   */
  public function assetPreview($mediaType, $id) {

    $metadatas = $this->searchService->getMediaMetadatas($id);
    $build = [];

    switch ($mediaType) {
      case 'image':
        $build = [
          '#theme' => 'keepeek_image_preview',
          '#title' => $metadatas['title'],
          '#attributes' => [
            'src' => ($metadatas['_links'][KeepeekMedia::METADATA_ATTRIBUTE_PREVIEW_STYLE]['href'] ?: $metadatas['permalink_original']),
            'title' => $metadatas['title'],
            'alt' => $metadatas['title'],
          ],
          '#width' => $metadatas['width'],
          '#height' => $metadatas['height'],
        ];
        break;

      case 'video':
        $build = [
          '#theme' => 'keepeek_video_modal',
          '#link' => $metadatas['permalink_original'],
          '#title' => $metadatas['title'],
        ];
        break;

      case 'document':
        $build = [
          '#theme' => 'image',
          '#title' => $metadatas['title'],
          '#attributes' => [
            'src' => $metadatas['permalink_original'],
          ],
        ];
        break;
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSession() {
    $response = new AjaxResponse();
    _media_keepeekdam_delete_session();
    $url = Url::fromRoute('media_keepeekdam.overview');
    $command = new RedirectCommand($url->toString());
    $response->addCommand($command);

    return $response;
  }

}
