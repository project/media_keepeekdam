<?php

namespace Drupal\media_keepeekdam\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media_keepeekdam\Service\SearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Provides the Keepeek search form interface.
 *
 * @internal
 */
class KeepeekSearchForm extends FormBase {

  /**
   * Keepeek search service.
   *
   * @var \Drupal\media_keepeekdam\Service\SearchService
   */
  protected $searchService;

  /**
   * KeepeekSearchForm constructor.
   *
   * {@inheritdoc}
   */
  public function __construct(SearchService $searchService) {
    $this->searchService = $searchService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('media_keepeekdam.search'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'keepeek_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Retrieve the chosen value from the media_library dialog.
    $selected_type = $this->getRequest()->query->get('media_library_selected_type');
    $default_keywords = $this->searchService->getStoredKeyword('q');
    $default_folder_id = $this->searchService->getStoredKeyword('fq');
    $default_folder_name = $this->searchService->getStoredKeyword('name_folder');
    if (!empty($default_folder_id)) {
      $default_folder_id = str_replace("folderId:", "", $default_folder_id);
    }
    $default_mediatype = 'image';
    if ($this->searchService->getStoredKeyword('f')) {
      [, $default_mediatype] = explode(':', $this->searchService->getStoredKeyword('f'));
    }
    if ($selected_type) {
      $default_mediatype = str_replace('keepeek_', '', $selected_type);
      $default_mediatype = $this->searchService->getMap($default_mediatype);
      $params = $this->searchService->getStoredKeyword();
      $params['f'] = 'mediaType:' . $default_mediatype;
      $this->searchService->setLastQuery($params);
    }

    $form['filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter Media'),
      '#open' => TRUE,
    ];
    $form['filters']['select_folder'] = [
      '#type' => 'html_tag',
      '#tag' => 'input',
      '#attributes' => [
        'type' => 'button',
        'value' => $this->t("Select a folder"),
        'class' => ['select-folder'],
        'name' => 'select_folder',
        'id' => 'select-folder',
      ],
      '#attached' => [
        'library' => [
          'media_keepeekdam/folder_tree',
        ],
      ],
    ];
    $form['filters']['keywords'] = [
      '#title' => $this->t('Keywords'),
      '#type' => 'textfield',
      '#default_value' => $default_keywords,
    ];
    // Remove "subtree" from value folder_id.
    $default_folder_id = str_replace('subtree', '', $default_folder_id);
    $default_folder_id = trim($default_folder_id);
    $form['filters']['folder_id'] = [
      '#title' => $this->t('folder_id'),
      '#type' => 'hidden',
      '#default_value' => $default_folder_id,
    ];

    $form['filters']['folder_name'] = [
      '#title' => $this->t('folder_name'),
      '#type' => 'hidden',
      '#default_value' => $default_folder_name,
    ];

    $form['filters']['mediatype'] = [
      '#title' => $this->t('Media Type'),
      '#type' => 'select',
      // @todo Integrate a mapping system.
      '#options' => [
        'image' => $this->t('Image'),
        'video' => $this->t('Video'),
        'application' => $this->t('Document'),
      ],
      '#default_value' => $default_mediatype,
    ];
    $form['filters']['folder_structure'] = [
      '#title' => $this->t('folder_structure'),
      '#type' => 'hidden',
    ];
    $form['filters']['actions'] = [
      '#type' => 'actions',
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];
    $form['#attributes']['class'][] = 'layout-column--half-form-filter';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Clear request params.
    $this->getRequest()->query->remove('page');
    $this->getRequest()->query->remove('media_library_selected_type');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $params['f'] = 'mediaType:' . $form_state->getValue('mediatype');
    if ($form_state->getValue('keywords')) {
      $params['q'] = $form_state->getValue('keywords');
    }
    // Add params "fq=folderId:" to search keepeekdam.
    if ($form_state->getValue('folder_id')) {
      $params['fq'] = 'folderId:' . $form_state->getValue('folder_id') . ' subtree';
    }
    if ($form_state->getValue('folder_name')) {
      $params['name_folder'] = $form_state->getValue('folder_name');
    }
    $this->searchService->setLastQuery($params);
    // Add "id" as param in url to make the scroll down after the filter.
    $url = Url::fromRoute('media_keepeekdam.overview', ['id' => 'edit-keepeek-media-table']);
    $form_state->setRedirectUrl($url);
  }

}
