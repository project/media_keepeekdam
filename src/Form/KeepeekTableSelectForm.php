<?php

namespace Drupal\media_keepeekdam\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\media_keepeekdam\Plugin\media\Source\KeepeekMedia;
use Drupal\media_keepeekdam\Service\SearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the table select form for the Keepeek result medias.
 *
 * @internal
 */
class KeepeekTableSelectForm extends FormBase {

  /**
   * Keepeek search service.
   *
   * @var \Drupal\media_keepeekdam\Service\SearchService
   */
  protected $searchService;

  /**
   * KeepeekdamConfig constructor.
   *
   * {@inheritdoc}
   */
  public function __construct(SearchService $searchService) {
    $this->searchService = $searchService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('media_keepeekdam.search'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'keepeek_table_select_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $current_page = $this->getRequest()->query->get('page') ?: 1;
    $last_query = (!is_null($this->searchService->getStoredKeyword()) ? $this->searchService->getStoredKeyword() : []);
    $result = $this->searchService->getSearchMedia('search/media', $last_query);
    if (isset($result['status']) && $result['status'] == '403') {
      $this->messenger()->addWarning($this->t('Permission denied.'));
    }
    elseif ($result['totalCount'] > 0) {
      if (isset($result['_embedded']['media'])) {
        if (!isset($result['_embedded']['media'][0])) {
          $backup = $result['_embedded']['media'];
          $result['_embedded']['media'] = [];
          $result['_embedded']['media'][0] = $backup;
        }
        foreach ($result['_embedded']['media'] as $media) {
          $timestamp = new DrupalDateTime($media['updateDate'], date_default_timezone_get());
          $already_imported = $this->searchService->checkExistMediaById($media['id']);
          [$type] = explode('/', $media['mediaType']);
          $attributes = [
            'target' => '_blank',
          ];
          $markup = new FormattableMarkup('<img src="@src" alt="@alt" title="@title" />', [
            '@src' => $media['_links'][KeepeekMedia::METADATA_ATTRIBUTE_THUMBNAIL_STYLE]['href'],
            '@alt' => $media['title'],
            '@title' => $media['title'],
          ]);
          $preview_link = Link::createFromRoute($markup, 'media_keepeekdam.asset_preview', [
            'id' => $media['id'],
            'mediaType' => $type,
          ], [
            'attributes' => $attributes,
          ])->toString();
          if ($type == 'application') {
            if ($this->searchService->getMediaPermalink($media['id'])) {
              $link = $this->searchService->getMediaPermalink($media['id']) . '?dl=true';
              $preview_link = Link::fromTextAndUrl($markup, Url::fromUri($link))->toString();
            }
            else {
              $preview_link = $markup;
            }
          }
          $options[$media['id']] = [
            'icon' => ($already_imported ? '✗' : ''),
            'keepeek_id' => $media['id'],
            'title' => $media['title'],
            'updateDate' => $timestamp->format('Y-m-d g:i a'),
            'type' => $media['mediaType'],
            'preview' => Markup::create($preview_link),
          ];
        }
      }
    }

    $header['icon'] = [
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['keepeek_id'] = $this->t('Keepeek ID');
    $header['title'] = [
      'data' => $this->t('Title'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['updateDate'] = [
      'data' => $this->t('Update date'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['type'] = [
      'data' => $this->t('Media Type'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['preview'] = [
      'data' => $this->t('Preview'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];

    $form['#attributes']['class'] = ['js-media-library-views-form'];
    $form['#attributes']['data-drupal-media-type'] = 'keepeek_image';
    $form['keepeek_media_table'] = [
      '#type' => 'tableselect',
      '#title' => $this->t('Keepeek'),
      '#header' => $header,
      '#options' => $options,
      '#empty' => $this->t('No Keepeek media available.'),
      '#multiple' => TRUE,
    ];
    $form['keepeek_media_table']['#prefix'] = $this->t('@totalCount Result(s)', ['@totalCount' => $result['totalCount']]);
    $form['keepeek_media_table']['#suffix'] = $this->searchService->paginateLinks($result['totalCount'], $current_page);
    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('keepeek_media_table', array_diff($form_state->getValue('keepeek_media_table'), [0]));
    if (count($form_state->getValue('keepeek_media_table')) == 0) {
      $form_state->setErrorByName('', $this->t('Select one or more Keepeek media to import.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ids = $form_state->getValue('keepeek_media_table');
    $result = $this->searchService->importKeepeekMedias($ids);
    switch ($result) {
      case 1:
        $this->messenger()->addStatus($this->t('The import has been performed.'));
        break;

      case 2:
        $this->messenger()->addWarning($this->t('You must have create permissions.'));
        break;

      case 3:
        $this->messenger()->addWarning($this->t('Problem on data recovery.'));
        break;
    }
    // Redirect to "Media Gallery" without param "id" (for scroll).
    $url = Url::fromRoute('media_keepeekdam.overview');
    $form_state->setRedirectUrl($url);
  }

}
