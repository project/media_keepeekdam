<?php

namespace Drupal\media_keepeekdam\Form;

use Drupal\Component\Utility\Tags;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media_keepeekdam\Service\ClientFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\Messenger;

/**
 * Class to generate the config form to Keepeek DAM.
 */
class KeepeekdamConfig extends ConfigFormBase {


  /**
   * Keepeek DAM client.
   *
   * @var \Drupal\media_keepeekdam\Service\ClientFactory
   */
  protected $keepeekDamClient;

  /**
   * Core messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * KeepeekdamConfig constructor.
   *
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, Messenger $messenger, ClientFactory $keepeekDamClient) {
    parent::__construct($config_factory);
    $this->messenger = $messenger;
    $this->keepeekDamClient = $keepeekDamClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('media_keepeekdam.client'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('media_keepeekdam.settings');

    $form['ui_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('UI configuration'),
    ];

    $form['ui_config']['global_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global name'),
      '#default_value' => $config->get('global_name'),
      '#description' => $this->t('This name will be used everywhere on the module interfaces.'),
      '#required' => TRUE,
    ];

    $form['ui_config']['results_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Results per page'),
      '#default_value' => $config->get('results_per_page'),
      '#description' => $this->t('The number of results per page on the search interface.'),
      '#required' => TRUE,
    ];

    $form['authentication'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Authentication details'),
    ];

    $form['authentication']['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URL'),
      '#default_value' => $config->get('base_url'),
      '#description' => $this->t('The base URL of the Keepeek DAM account to use for API access.'),
      '#required' => TRUE,
    ];

    $form['authentication']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get('username'),
      '#description' => $this->t('The username of the Keepeek DAM account to use for API access.'),
      '#required' => TRUE,
    ];

    $form['authentication']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('password'),
      '#description' => $this->t('The password of the Keepeek DAM account to use for API access. Note that this field will appear blank even if you have previously saved a value.'),
      '#required' => ($config->get('password') ? FALSE : TRUE),
    ];

    $form['authentication']['curl_timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Curl timeout'),
      '#default_value' => $config->get('curl_timeout'),
      '#description' => $this->t('Timeout value to use when making curl calls.'),
      '#required' => TRUE,
    ];

    $form['api_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Api Configuration'),
    ];

    $form['api_config']['common_media_metadatas'] = [
      '#title' => $this->t('Metadatas for common Keepeek media'),
      '#type' => 'textarea',
      '#description' => $this->t('Use a case-sensitive, comma-separated list of phrases. Example: keywords, location'),
      '#default_value' => Tags::implode($config->get('common_media_metadatas')),
    ];

    $form['api_config']['image_media_metadatas'] = [
      '#title' => $this->t('Metadatas for Keepeek image media'),
      '#type' => 'textarea',
      '#description' => $this->t('Use a case-sensitive, comma-separated list of phrases.
      Example: keywords|field_keywords, location|field_location'),
      '#default_value' => Tags::implode($config->get('image_media_metadatas')),
    ];

    $form['api_config']['video_media_metadatas'] = [
      '#title' => $this->t('Metadatas for Keepeek video media'),
      '#type' => 'textarea',
      '#description' => $this->t('Use a case-sensitive, comma-separated list of phrases.
      Example: keywords|field_keywords, location|field_location'),
      '#default_value' => Tags::implode($config->get('video_media_metadatas')),
    ];

    $form['api_config']['document_media_metadatas'] = [
      '#title' => $this->t('Metadatas for Keepeek doc media'),
      '#type' => 'textarea',
      '#description' => $this->t('Use a case-sensitive, comma-separated list of phrases.
      Example: keywords|field_keywords, location|field_location'),
      '#default_value' => Tags::implode($config->get('document_media_metadatas')),
    ];

    $form['api_config']['image_styles_mapping'] = [
      '#title' => $this->t('Image styles mapping'),
      '#type' => 'textarea',
      '#description' => $this->t('Mapping between Keepeek and Drupal image styles. Use a case-sensitive, comma-separated list of phrases.
      Example: kpk:style1|style_1, kpk:style2|style_2'),
      '#default_value' => Tags::implode($config->get('image_styles_mapping')),
    ];

    $form['api_config']['cdn_keepeek'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDN Keepeek'),
      '#default_value' => $config->get('cdn_keepeek'),
      '#description' => $this->t('The domain of the Keepeek CDN without the schema.
      Example: assets1.keepeek.com'),
    ];

    $form['api_config']['cdn_keepeek_video_player'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDN Keepeek Video Player'),
      '#default_value' => $config->get('cdn_keepeek_video_player'),
      '#description' => $this->t('The domain of the Keepeek CDN Video Player without the schema.
      Example: assets1.keepeek.com/video'),
    ];

    $form['api_config']['id_published_status'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID published status'),
      '#default_value' => $config->get('id_published_status'),
      '#description' => $this->t('ID published custom status.'),
    ];

    $form['api_config']['id_ready_for_publication_status'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID ready for publication status'),
      '#default_value' => $config->get('id_ready_for_publication_status'),
      '#description' => $this->t('ID ready for publication custom status.'),
    ];

    $form['cron_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cron configuration'),
    ];

    $form['cron_config']['refresh_interval'] = [
      '#title' => $this->t('Refresh interval'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('refresh_interval'),
      '#description' => $this->t('Value in seconds. -1 interval means run on every cron.'),
    ];
    // URl configuration.
    $form['url_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('URL configuration'),
    ];
    $form['url_config']['prefix'] = [
      '#title' => $this->t('Prefix url'),
      '#type' => 'textfield',
      '#default_value' => $config->get('prefix'),
      '#description' => $this->t('Prefix Media Gallery folder URL.'),
    ];
    $form['url_config']['suffix'] = [
      '#title' => $this->t('Suffix url'),
      '#type' => 'textfield',
      '#default_value' => $config->get('suffix'),
      '#description' => $this->t('Suffix Media Gallery folder URL.'),
    ];

    $form['proxy'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Proxy configuration'),
    ];

    $form['proxy']['proxy_status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Proxy status'),
      '#options' => [
        1 => $this->t('Enable'),
        0 => $this->t('Disable'),
      ],
      '#default_value' => $config->get('proxy_status'),
      '#description' => $this->t('Status proxy configuration.'),
      '#required' => TRUE,
    ];

    $form['proxy']['proxy_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy URL'),
      '#default_value' => $config->get('proxy_url'),
      '#description' => $this->t('The proxy URL configuration.'),
      '#states' => [
        'visible' => [
          ':input[name="proxy_status"]' => ['value' => 1],
        ],
        'required' => [
          ':input[name="proxy_status"]' => ['value' => 1],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'keepeekdam_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_keepeekdam.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // We set the client data array with the values from form_state.
    $base_url = $form_state->getValue('base_url');
    $username = $form_state->getValue('username');
    $password = $this->getFieldValue($form_state, 'password');
    $form_state->setValue('password', $password);
    $proxy_status = $form_state->getValue('proxy_status');
    $proxy_url = $form_state->getValue('proxy_url');
    $params = [
      'base_url' => $base_url,
      'username' => $username,
      'password' => $password,
      'proxy_status' => $proxy_status,
      'proxy_url' => $proxy_url,
    ];
    try {
      $checkClient = $this->keepeekDamClient->getClient($params);
      $checkRequest = $checkClient->request('GET', '/api/dam');
      if ($checkRequest->getStatusCode() === 200 && $checkRequest->getReasonPhrase() == 'OK') {
        $this->messenger->addStatus($this->t('Successfully connected to Keepeek API'));
      }
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('authentication', $this->t('Failed to connect to Keepeek API. The server reports the following message: %error.', ['%error' => $e->getMessage()]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('media_keepeekdam.settings')
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('username', $form_state->getValue('username'))
      ->set('password', $form_state->getValue('password'))
      ->set('curl_timeout', $form_state->getValue('curl_timeout'))
      ->set('proxy_status', $form_state->getValue('proxy_status'))
      ->set('proxy_url', $form_state->getValue('proxy_url'))
      ->set('common_media_metadatas', Tags::explode($form_state->getValue('common_media_metadatas')))
      ->set('image_media_metadatas', Tags::explode($form_state->getValue('image_media_metadatas')))
      ->set('video_media_metadatas', Tags::explode($form_state->getValue('video_media_metadatas')))
      ->set('document_media_metadatas', Tags::explode($form_state->getValue('document_media_metadatas')))
      ->set('image_styles_mapping', Tags::explode($form_state->getValue('image_styles_mapping')))
      ->set('results_per_page', $form_state->getValue('results_per_page'))
      ->set('cdn_keepeek', $form_state->getValue('cdn_keepeek'))
      ->set('cdn_keepeek_video_player', $form_state->getValue('cdn_keepeek_video_player'))
      ->set('refresh_interval', $form_state->getValue('refresh_interval'))
      ->set('global_name', $form_state->getValue('global_name'))
      ->set('prefix', $form_state->getValue('prefix'))
      ->set('suffix', $form_state->getValue('suffix'))
      ->set('id_published_status', $form_state->getValue('id_published_status'))
      ->set('id_ready_for_publication_status', $form_state->getValue('id_ready_for_publication_status'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Gets a form value from stored config.
   *
   * @param string $field_name
   *   The key of the field in the simple config.
   *
   * @return mixed
   *   The value for the given form field, or NULL.
   */
  protected function getFormValueFromConfig($field_name) {
    $config_name = $this->getEditableConfigNames();
    $value = $this->config(reset($config_name))->get($field_name);

    return $value;
  }

  /**
   * Gets a form field value, either from the form or from config.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param string $field_name
   *   The key of the field in config. (This may differ from form field key).
   *
   * @return mixed
   *   The value for the given form field, or NULL.
   */
  protected function getFieldValue(FormStateInterface $form_state, $field_name) {
    // If the user has entered a value use it, if not check config.
    $value = $form_state->getValue($field_name) ?: $this->getFormValueFromConfig($field_name);
    return $value;
  }

}
