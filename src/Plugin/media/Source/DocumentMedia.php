<?php

namespace Drupal\media_keepeekdam\Plugin\media\Source;

/**
 * External Keepeek entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "keepeek_document",
 *   label = @Translation("Keepeek Document"),
 *   description = @Translation("Use remote Keepeek documents."),
 *   allowed_field_types = {"string_long"},
 *   thumbnail_alt_metadata_attribute = "title",
 *   default_thumbnail_filename = "generic.png"
 * )
 */
class DocumentMedia extends KeepeekMedia {

  /**
   * Key for "PageCount" metadata attribute.
   */
  const METADATA_ATTRIBUTE_PAGE_COUNT = 'pageCount';

  /**
   * Get Metadata Attributes.
   */
  public function getMetadataAttributes() {
    $additionals = parent::getAdditionalsMetadataAttributes('document_media_metadatas');

    return parent::getMetadataAttributes() + [
      static::METADATA_ATTRIBUTE_PAGE_COUNT => $this->t('Page Count'),
    ] + $additionals;
  }

}
