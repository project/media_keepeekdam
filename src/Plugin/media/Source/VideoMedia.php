<?php

namespace Drupal\media_keepeekdam\Plugin\media\Source;

/**
 * External Keepeek entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "keepeek_video",
 *   label = @Translation("Keepeek Video"),
 *   description = @Translation("Use remote Keepeek videos."),
 *   allowed_field_types = {"string_long"},
 *   thumbnail_alt_metadata_attribute = "title",
 *   default_thumbnail_filename = "no-thumbnail.png"
 * )
 */
class VideoMedia extends KeepeekMedia {

  /**
   * Key for "Width" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_WIDTH = 'width';

  /**
   * Key for "Height" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_HEIGHT = 'height';

  /**
   * Key for "Resolution" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_RESOLUTION = 'resolution';

  /**
   * Key for "Duration" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_DURATION = 'duration';

  /**
   * Get Metadata Attributes.
   */
  public function getMetadataAttributes() {
    $additionals = parent::getAdditionalsMetadataAttributes('video_media_metadatas');

    return parent::getMetadataAttributes() + [
      static::METADATA_ATTRIBUTE_WIDTH => $this->t('Width'),
      static::METADATA_ATTRIBUTE_HEIGHT => $this->t('Height'),
      static::METADATA_ATTRIBUTE_RESOLUTION => $this->t('Resolution'),
      static::METADATA_ATTRIBUTE_DURATION => $this->t('Duration'),
    ] + $additionals;
  }

}
