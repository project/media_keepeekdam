<?php

namespace Drupal\media_keepeekdam\Plugin\media\Source;

/**
 * External Keepeek entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "keepeek_image",
 *   label = @Translation("Keepeek Image"),
 *   description = @Translation("Use remote Keepeek images."),
 *   allowed_field_types = {"string_long"},
 *   thumbnail_alt_metadata_attribute = "title",
 *   default_thumbnail_filename = "no-thumbnail.png"
 * )
 */
class ImageMedia extends KeepeekMedia {

  /**
   * Key for "Width" metadata attribute.
   */
  const METADATA_ATTRIBUTE_WIDTH = 'width';

  /**
   * Key for "Height" metadata attribute.
   */
  const METADATA_ATTRIBUTE_HEIGHT = 'height';

  /**
   * Key for "Resolution" metadata attribute.
   */
  const METADATA_ATTRIBUTE_RESOLUTION = 'resolution';

  /**
   * Get Metadata Attributes.
   *
   * @return array
   *   Get metadata attributes.
   */
  public function getMetadataAttributes() {
    $additionals = parent::getAdditionalsMetadataAttributes('image_media_metadatas');

    return parent::getMetadataAttributes() + [
      static::METADATA_ATTRIBUTE_WIDTH => $this->t('Width'),
      static::METADATA_ATTRIBUTE_HEIGHT => $this->t('Height'),
      static::METADATA_ATTRIBUTE_RESOLUTION => $this->t('Resolution'),
    ] + $additionals;
  }

}
