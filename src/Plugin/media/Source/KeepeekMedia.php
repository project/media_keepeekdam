<?php

namespace Drupal\media_keepeekdam\Plugin\media\Source;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media_keepeekdam\Service\ClientFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Keepeek entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "keepeek_media",
 *   label = @Translation("Keepeek Media"),
 *   description = @Translation("Use external media from Keepeek DAM."),
 *   allowed_field_types = {"string_long"},
 *   thumbnail_alt_metadata_attribute = "title",
 *   default_thumbnail_filename = "generic.png"
 * )
 */
class KeepeekMedia extends MediaSourceBase {

  /**
   * Key for "Id" metadata attribute.
   */
  const METADATA_ATTRIBUTE_ID = 'id';

  /**
   * Key for "Title" metadata attribute.
   */
  const METADATA_ATTRIBUTE_TITLE = 'title';

  /**
   * Key for "MediaType" metadata attribute.
   */
  const METADATA_ATTRIBUTE_MEDIA_TYPE = 'mediaType';

  /**
   * Key for "FileSize" metadata attribute.
   */
  const METADATA_ATTRIBUTE_FILE_SIZE = 'fileSize';

  /**
   * Key for "CreationDate" metadata attribute.
   */
  const METADATA_ATTRIBUTE_CREATION_DATE = 'creationDate';

  /**
   * Key for "UpdateDate" metadata attribute.
   */
  const METADATA_ATTRIBUTE_UPDATE_DATE = 'updateDate';

  /**
   * Key for "ImportDate" metadata attribute.
   */
  const METADATA_ATTRIBUTE_IMPORT_DATE = 'importDate';

  /**
   * Key for "ThumbnailStyle" metadata attribute.
   */
  const METADATA_ATTRIBUTE_THUMBNAIL_STYLE = 'kpk:medium';
  const METADATA_ATTRIBUTE_PREVIEW_STYLE = 'preview';

  /**
   * The HTTP client.
   *
   * @var \Drupal\media_keepeekdam\Service\ClientFactory
   */
  protected $clientFactory;

  /**
   * Constructs a new Keepeek Media instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\media_keepeekdam\Service\ClientFactory $clientFactory
   *   The HTTP client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config_factory, FieldTypePluginManagerInterface $field_type_manager, ClientFactory $clientFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->clientFactory = $clientFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('media_keepeekdam.client')
    );
  }

  /**
   * Get Metadata Attributes.
   *
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    $additionals = self::getAdditionalsMetadataAttributes('common_media_metadatas');

    return [
      static::METADATA_ATTRIBUTE_ID => $this->t('Keepeek ID'),
      static::METADATA_ATTRIBUTE_TITLE => $this->t('Title'),
      static::METADATA_ATTRIBUTE_MEDIA_TYPE => $this->t('Media Type'),
      static::METADATA_ATTRIBUTE_FILE_SIZE => $this->t('File size'),
      static::METADATA_ATTRIBUTE_CREATION_DATE => $this->t('Creation date'),
      static::METADATA_ATTRIBUTE_UPDATE_DATE => $this->t('Update date'),
      static::METADATA_ATTRIBUTE_IMPORT_DATE => $this->t('Import date'),
      static::METADATA_ATTRIBUTE_THUMBNAIL_STYLE => $this->t('Thumbnail'),
    ] + $additionals;
  }

  /**
   * Get Metadata.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media object.
   * @param string $attribute_name
   *   Attribute name.
   *
   * @return mixed|string|null
   *   Get metadate value.
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    // Get the text_long field where the JSON object is stored.
    $remote_field = $media->get($this->configuration['source_field']);
    // If the source field is not required, it may be empty.
    if (!$remote_field) {
      return parent::getMetadata($media, $attribute_name);
    }
    $parsed_data = $this->clientFactory->parseData($remote_field->value, self::getMetadataAttributes());
    switch ($attribute_name) {
      // This is used to set the name of the media if is blank.
      case 'default_name':
        $return = $parsed_data[static::METADATA_ATTRIBUTE_TITLE];
        break;

      // This is used to generate the thumbnail field.
      case 'thumbnail_uri':
        $return = $parsed_data[static::METADATA_ATTRIBUTE_THUMBNAIL_STYLE];
        break;

      default:
        $return = $parsed_data[$attribute_name] ?? parent::getMetadata($media, $attribute_name);
        break;
    }

    return $return;
  }

  /**
   * Get Additionals Metadata Attributes.
   *
   * @return array
   *   List of additionals metadata attributes.
   */
  public function getAdditionalsMetadataAttributes($type) {
    $additionals = [];
    $metadatas = $this->configFactory->get('media_keepeekdam.settings')->get($type);
    if (!empty($metadatas)) {
      foreach ($metadatas as $metadata) {
        $metadata_parts = explode('|', $metadata);
        $additionals[$metadata_parts[0]] = $this->t('%label', ['%label' => Unicode::ucfirst($metadata_parts[0])]);
      }
    }

    return $additionals;
  }

}
