<?php

namespace Drupal\media_keepeekdam\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\media_keepeekdam\Service\KeepeekMedia;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'keepeek_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "keepeek_formatter",
 *   module = "media_keepeekdam",
 *   label = @Translation("Keepeek formatter"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class KeepeekFormatter extends FormatterBase {


  /**
   * Keepeek Media service.
   *
   * @var \Drupal\media_keepeekdam\Service\KeepeekMedia
   */
  protected $keepeekMedia;

  /**
   * Constructs a KeepeekFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\media_keepeekdam\Service\KeepeekMedia $keepeek_media
   *   The Keepeek Media Service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, KeepeekMedia $keepeek_media) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->keepeekMedia = $keepeek_media;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('media_keepeekdam.media')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'columns' => 6,
      'ratio' => '16-9',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return parent::settingsForm($form, $form_state) + [
      'columns' => [
        '#type' => 'select',
        '#title' => $this->t('Number columns'),
        '#default_value' => $this->getSetting('columns'),
        '#options' => [
          1 => $this->t('1 column'),
          2 => $this->t('2 columns'),
          3 => $this->t('3 columns'),
          4 => $this->t('4 columns'),
          5 => $this->t('5 columns'),
          6 => $this->t('6 columns'),
          7 => $this->t('7 columns'),
          8 => $this->t('8 columns'),
          9 => $this->t('9 columns'),
          10 => $this->t('10 columns'),
          11 => $this->t('11 columns'),
          12 => $this->t('12 columns'),
        ],
      ],
      'ratio' => [
        '#type' => 'select',
        '#title' => $this->t('Ratio'),
        '#default_value' => $this->getSetting('ratio'),
        '#options' => [
          '16-9' => $this->t('16:9'),
          '4-3' => $this->t('4:3'),
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($this->getSetting('columns')) {
      $summary[] = $this->t('Number of columns: %columns', [
        '%columns' => $this->getSetting('columns'),
      ]);
    }
    if ($this->getSetting('ratio')) {
      $summary[] = $this->t('Ratio : %ratio', [
        '%ratio' => $this->getSetting('ratio'),
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $columns = $this->getSetting('columns');
    $ratio = $this->getSetting('ratio');
    $media_object = $items->getParent()->getValue();
    $caption = $media_object->get('field_description')->getValue();
    if (isset($caption[0]['value'])) {
      $caption = nl2br(Html::escape($caption[0]['value']));
    }
    $alt = $media_object->get('field_alt')->getString();
    $title = $media_object->get('field_title')->getString();
    $filesize = $media_object->get('field_filesize')->getString();
    $bundle = $items->getFieldDefinition()->getTargetBundle();
    foreach ($items as $delta => $item) {
      $original_uri = NULL;
      $uri = $item->getUrl()->getUri();
      if ($bundle == 'keepeek_video') {
        $json = $item->getEntity()->get('field_media_keepeek_video')->value;
        $values = Json::decode($json);
        if (isset($values['public_share_link'])) {
          $uri = $values['public_share_link'];
          $original_uri = $values['permalink_original'];
        }
      }

      if ($bundle == 'keepeek_document') {
        $uri .= '?dl=true';
      }
      $uri_with_cdn = $this->keepeekMedia->applyCdnKeepeek($uri, $bundle);
      // If URI with CDN different, update URL.
      if ($uri_with_cdn != $uri) {
        $url = Url::fromUri($uri_with_cdn);
      }
      else {
        $url = Url::fromUri($uri);
      }
      $elements[$delta] = [
        '#theme' => $bundle . '_formatter',
        '#columns' => $columns ?: '6',
        '#title' => $title,
        '#ratio' => $ratio ?: '16-9',
        '#alt' => $alt,
        '#caption' => $caption ?: '',
        '#filesize' => $filesize ? \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.2.0', fn() => \Drupal\Core\StringTranslation\ByteSizeMarkup::create($filesize), fn() => format_size($filesize)) : '',
        '#link' => $url,
        '#original_link' => $original_uri,
        '#mid' => $item->getEntity()->id(),
        '#kid' => $item->getEntity()->get('field_keepeek_id')->getString(),
      ];
    }

    return $elements;
  }

}
