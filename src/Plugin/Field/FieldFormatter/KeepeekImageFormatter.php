<?php

namespace Drupal\media_keepeekdam\Plugin\Field\FieldFormatter;

use Drupal\breakpoint\BreakpointInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\media_keepeekdam\Service\KeepeekMedia;
use Drupal\media_keepeekdam\Service\ResponsiveKeepeekMedia;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'keepeek_image_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "keepeek_image_formatter",
 *   module = "media_keepeekdam",
 *   label = @Translation("Keepeek image formatter"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class KeepeekImageFormatter extends KeepeekFormatter implements ContainerFactoryPluginInterface {

  /**
   * Search service.
   *
   * @var \Drupal\media_keepeekdam\Service\ResponsiveKeepeekMedia
   */
  protected $responsiveKeepeekMedia;

  /**
   * The responsive image style entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $responsiveImageStyleStorage;

  /**
   * Constructs a KeepeekImageFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\media_keepeekdam\Service\ResponsiveKeepeekMedia $responsiveKeepeekMedia
   *   The search service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $responsive_image_style_storage
   *   The responsive image style storage.
   * @param \Drupal\media_keepeekdam\Service\KeepeekMedia $keepeek_media
   *   The Keepeek Media service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ResponsiveKeepeekMedia $responsiveKeepeekMedia, EntityStorageInterface $responsive_image_style_storage, KeepeekMedia $keepeek_media) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $keepeek_media);
    $this->responsiveKeepeekMedia = $responsiveKeepeekMedia;
    $this->responsiveImageStyleStorage = $responsive_image_style_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('media_keepeekdam.responsive'),
      $container->get('entity_type.manager')->getStorage('responsive_image_style'),
      $container->get('media_keepeekdam.media')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {

    return [
      'responsive_image_style' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $options = $this->responsiveKeepeekMedia->getResponsiveImageStylesOptions();

    return parent::settingsForm($form, $form_state) + [
      'responsive_image_style' => [
        '#type' => 'select',
        '#title' => $this->t('Responsive image Style'),
        '#default_value' => $this->getSetting('responsive_image_style'),
        '#options' => $options,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($this->getSetting('responsive_image_style')) {
      $summary[] = $this->t('Responsive image style: %responsive_image_style', [
        '%responsive_image_style' => $this->getSetting('responsive_image_style'),
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $mapping = $this->responsiveKeepeekMedia->getImageStylesMapping();
    foreach ($elements as &$element) {
      $mid = $element['#mid'];
      $variables['mid'] = $mid;
      $variables['kid'] = $element['#kid'];
      $global_path = $element['#link'];
      if ($element['#link'] instanceof Url) {
        $variables['uri'] = $global_path->toUriString();
        $responsive_image_style = $this->responsiveImageStyleStorage->load($this->getSetting('responsive_image_style'));
        if ($responsive_image_style) {
          // Get dynamic breakpoints of theme.
          $breakpoints = array_reverse(\Drupal::service('breakpoint.manager')->getBreakpointsByGroup($responsive_image_style->getBreakpointGroup()));
          foreach ($responsive_image_style->getKeyedImageStyleMappings() as $breakpoint_id => $multipliers) {
            if (isset($breakpoints[$breakpoint_id]) && $breakpoints[$breakpoint_id] instanceof BreakpointInterface) {
              foreach ($multipliers as $multiplier => $image_style_mapping) {
                $srcset[intval(mb_substr($multiplier, 0, -1) * 100)] = $variables['uri'] . ' ' . $multiplier;
              }
              $variables['sources'][] = $this->responsiveKeepeekMedia->responsiveKeepeekImageBuildSourceAttributes($variables, $breakpoints[$breakpoint_id], $multipliers);
            }
          }
          $keepeek_style = $mapping[$image_style_mapping['image_mapping']];
          $variables['img_element'] = [
            '#theme' => 'image',
            '#uri' => $this->responsiveKeepeekMedia->buildKeepeekImageStyle($mid, $keepeek_style),
            '#title' => $element['#title'],
            '#alt' => $element['#alt'],
          ];
        }
      }
      $element['#variables'] = $variables;
    }

    return $elements;
  }

}
