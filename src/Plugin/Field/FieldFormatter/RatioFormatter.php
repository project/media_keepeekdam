<?php

namespace Drupal\media_keepeekdam\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'ratio_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "ratio_formatter",
 *   label = @Translation("Ratio Formatter"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class RatioFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $data = Json::decode($item->value);
      $value = $this->t('Width: %width px / Height: %height px', [
        '%width' => $data['width'],
        '%height' => $data['height'],
      ]);
      $elements[$delta] = [
        '#markup' => '<div class="ratio-keepeek">' . $value . '</div>',
        '#attached' => [
          'library' => [
            'media_keepeekdam/media_library',
          ],
        ],
      ];
    }

    return $elements;
  }

}
