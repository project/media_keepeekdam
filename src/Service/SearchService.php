<?php

namespace Drupal\media_keepeekdam\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * API SEARCH.
 */
class SearchService extends ClientFactory {
  use StringTranslationTrait;

  /**
   * The map between migrate status and watchdog severity.
   *
   * @var array
   */
  protected const MAP = [
    'image' => 'image',
    'video' => 'video',
    'application' => 'document',
  ];

  /**
   * The public share link key.
   */
  protected const PUBLIC_SHARE_LINK = 'kpk:link';

  /**
   * Original permalink key.
   */
  protected const ORIGINAL_PERMALINK = 'latest-version';

  /**
   * Links key.
   */
  protected const LINKS_KEY = '_links';

  /**
   * Embedded key.
   */
  protected const EMBEDDED_KEY = '_embedded';

  /**
   * Methods MAP.
   */
  protected const METHOD_SAVE_LAST_QUERY = [
    'medias' => 'medias/',
    'media_search' => 'search/media',
    'publish' => 'medias/',
    'path_status' => 'medias/custom-statuses/',
    'medias_by_folder' => 'folders/%folderId/medias',
  ];

  /**
   * Constants pager.
   */
  protected const START_COUNTER = 1;
  protected const END_COUNTER = 10;
  protected const LIMIT_START_COUNTER = 5;
  protected const LIMIT_END_COUNTER = 9;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current logged user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The media storage.
   *
   * @var \Drupal\media_keepeekdam\Service\AccessCheck
   */
  protected $access;

  /**
   * Client constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity query service.
   * @param \Drupal\media_keepeekdam\Service\AccessCheck $access
   *   The Access check service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user connected.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager, AccessCheck $access, EntityFieldManagerInterface $entity_field_manager, AccountProxyInterface $current_user = NULL) {
    parent::__construct($config_factory, $logger_factory);
    $this->currentUser = $current_user;
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
    $this->access = $access;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * APISearch: dynamic function to get media.
   *
   * @param string $method_link
   *   uri url call function like 'search/media' or 'search/media/{mediaId}'...
   * @param array $params
   *   Contains all the parameters to be sent like :['page' => 1, 'size' => 20].
   *
   * @return array
   *   Array contains medias.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getSearchMedia(string $method_link, array $params = []) {
    try {
      $page = $this->requestStack->getCurrentRequest()->query->get('page') ?: 1;
      $params['page'] = $page;
      $params['size'] = $this->config->get('results_per_page');
      $params['sort'] = 'updateDate+desc';
      // @todo $params['sort'] = 'asc';
      // @todo $params['order'] = 'updateDate';
      if (isset($params['f'])) {
        $type = str_replace('mediaType:', '', $params['f']);
        if ($this->access->keepeekImageMediaAccess(self::MAP[$type], 'search') instanceof AccessResultAllowed) {
          $response = $this->runQuery('GET', $method_link, [
            'query' => http_build_query($params),
          ]);
          if ($response && $response->getStatusCode() == Response::HTTP_OK) {
            // Save the last query for the current user.
            if (in_array($method_link, self::METHOD_SAVE_LAST_QUERY)) {
              $this->setLastQuery($params);
            }

            return Json::decode($response->getBody());
          }
        }

        return ['status' => '403'];
      }
    }
    catch (\Exception $e) {
      $current_url = Url::fromRoute('<current>', [], ["absolute" => TRUE])->toString();
      $error_message = $e->getMessage();
      $this->setError($error_message, $current_url);

      return ['error' => $error_message, 'status' => $e->getCode()];
    }
  }

  /**
   * Set the last query executed by user.
   *
   * @param array $params
   *   The list of params to save.
   */
  public function setLastQuery(array $params = []) {
    $this->requestStack
      ->getCurrentRequest()
      ->getSession()
      ->set('keepeek_last_query', $params);
  }

  /**
   * Get stored Keyword.
   *
   * @param string $param
   *   Param to return.
   *
   * @return array|mixed|string|string[]
   *   Value of stored params.
   */
  public function getStoredKeyword(string $param = NULL) {
    $param_value = '';
    $last_query = $this->requestStack
      ->getCurrentRequest()
      ->getSession()
      ->get('keepeek_last_query');
    if (!$last_query) {
      $last_query['f'] = 'mediaType:image';
    }
    if (!is_null($param)) {
      if (isset($last_query[$param])) {
        $param_value = $last_query[$param];
      }
    }

    return is_null($param) ? $last_query : $param_value;
  }

  /**
   * Generate pager links.
   *
   * @param int $total_results
   *   Total count of results.
   * @param int $current_page
   *   The current page number.
   *
   * @todo Improve generation of pager links when we have many pages.
   *
   * @return string
   *   Render pager links.
   */
  public function paginateLinks($total_results = 0, $current_page = 1) {
    $start_counter = self::START_COUNTER;
    $end_counter = self::END_COUNTER;
    $total_pages = ceil($total_results / $this->config->get('results_per_page'));

    // If only one page then no point in showing a single paginated link.
    if ($total_pages <= 1) {
      return '';
    }

    $render = '<nav class="pager" role="navigation" aria-labelledby="pagination-heading">
        <h4 id="pagination-heading" class="pager__heading visually-hidden">' . $this->t('Pagination') . '</h4>
                    <ul class="pager__items js-pager__items">';

    $render .= '<li class="pager__item pager__item--action pager__item--first">
				<a href="?page=' . $start_counter . '" class="pager__link pager__link--action-link" title="Go to first page">
				<span class="visually-hidden">' . $this->t('First Page') . '</span>
				<span class="pager__item-title pager__item-title--backwards" aria-hidden="true">' . $this->t('First') . '</span></a>
				</li>';

    // If current page > 1 only then show previous page.
    if ($current_page > 1) {
      $render .= '<li class="pager__item pager__item--action pager__item--previous">
                    <a href="?page=' . ($current_page - 1) . '" class="pager__link pager__link--action-link" aria-label="Previous">
                    <span class="visually-hidden">' . $this->t('Previous page') . '</span>
                    <span class="pager__item-title pager__item-title--backwards" aria-hidden="true">' . $this->t('Previous') . '</span>
                    </a>
                  </li>';
    }

    if ($current_page > self::LIMIT_START_COUNTER) {
      $start_counter = $current_page - self::LIMIT_START_COUNTER;
      $end_counter = $start_counter + self::LIMIT_END_COUNTER;
    }
    if ($total_pages < $end_counter) {
      $end_counter = $total_pages;
    }
    // Middle pages.
    for ($i = $start_counter; $i <= $end_counter; $i++) {
      if ($i == $current_page) {
        $render .= '<li class="pager__item pager__item--number is-active"><a href="#" class="pager__link is-active"><span class="visually-hidden">' . $this->t('Current page') . '</span>' . $i . '</a></li>';
      }
      else {
        $render .= '<li class="pager__item pager__item--number">
          <a href="?page=' . $i . '" class="pager__link"><span class="visually-hidden">' . $this->t('Page') . ' ' . $i . '</span>' . $i . '</a>
        </li>';

      }
    }

    // If current page is not last page then only show next page link.
    if ($current_page != $total_pages) {
      $render .= '<li class="pager__item pager__item--action pager__item--next">
                    <a href="?page=' . ($current_page + 1) . '" class="pager__link pager__link--action-link" aria-label="Next">
                    <span class="visually-hidden">' . $this->t('Next page') . '</span>
                    <span class="pager__item-title pager__item-title--forward" aria-hidden="true">' . $this->t('Next') . '</span>
                    </a>
                  </li>';

    }

    $render .= '<li class="pager__item pager__item--action pager__item--last">
          <a href="?page=' . $total_pages . '" class="pager__link pager__link--action-link" title="Go to last page"><span class="visually-hidden">' . $this->t('Last Page') . '</span><span class="pager__item-title pager__item-title--forward" aria-hidden="true">' . $this->t('Last') . '</span></a>
          </li>';

    $render .= '</ul></nav>';

    return $render;
  }

  /**
   * Get Metadats of media.
   *
   * @param int $id
   *   Id of media selected.
   */
  public function getMediaMetadatas(int $id) {
    $return = [];
    $response = $this->runQuery('GET', self::METHOD_SAVE_LAST_QUERY['medias'] . $id);
    if ($response && $response->getStatusCode() == Response::HTTP_OK) {
      $return = Json::decode($response->getBody());
      // Only for Keepeek media video, get the public share link.
      if (strpos($return['mediaType'], 'video') !== FALSE) {
        $return['public_share_link'] = $this->getPublicShareLink($id);
      }
      $return['permalink_original'] = $this->getMediaPermalink($id);
    }

    return $return;
  }

  /**
   * Get permalink of media.
   *
   * @param int $id
   *   Id of media selected.
   * @param string $type
   *   Type of permalink.
   *
   * @return mixed|string
   *   Url of permalink.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getMediaPermalink(int $id, string $type = 'original') {
    $response = $this->runQuery('GET', self::METHOD_SAVE_LAST_QUERY['medias'] . $id . '/permalinks/' . $type);
    $return = '';
    if ($response && $response->getStatusCode() == Response::HTTP_OK) {
      $data = Json::decode($response->getBody());
      if (isset($data[self::LINKS_KEY][self::ORIGINAL_PERMALINK])) {
        $return = $data[self::LINKS_KEY][self::ORIGINAL_PERMALINK]['href'];
      }
    }

    return $return;
  }

  /**
   * Import a list of media selected.
   *
   * @param array $ids
   *   Ids of media to import.
   */
  public function importKeepeekMedias(array $ids) {
    $common_filters = $this->config->get('common_media_metadatas');
    foreach ($ids as $id) {
      $meta_datas = $this->getMediaMetadatas($id);
      if (!empty($meta_datas)) {
        [$type] = explode('/', $meta_datas['mediaType']);
        $type = $this->getMap($type, TRUE);
        if (!empty($type)) {
          if ($this->access->keepeekImageMediaAccess($type, 'create') instanceof AccessResultAllowed) {
            $bundle = 'keepeek_' . $type;
            $field = 'field_media_keepeek_' . $type;
            $specific_filters = $this->config->get($type . '_media_metadatas');
            $all_filters = array_merge($specific_filters, $common_filters);
            $meta_datas_to_import = $this->filterMetadatas($meta_datas, $all_filters);

            // Get bundle fields configs.
            $bundle_fields = $this->entityFieldManager->getFieldDefinitions('media', $bundle);
            // Cleanup data according fields configs.
            foreach ($meta_datas_to_import as $field_key => &$data) {
              if (isset($bundle_fields[$field_key])) {
                $field_definition = $bundle_fields[$field_key];
                $max_length = $field_definition->getSetting('max_length');
                // If the data exceeds the authorized size so doing a trucate.
                if (is_numeric($max_length) && mb_strlen($data) > $max_length) {
                  $data = Unicode::truncate($data, $max_length);
                }
              }
            }

            $media = Media::create([
              'bundle' => $bundle,
              $field => Json::encode($meta_datas),
              'name' => $meta_datas['title'],
              'field_keepeek_id' => $meta_datas['id'],
              'field_filesize' => $meta_datas['fileSize'],
              'field_original_permalink' => $meta_datas['permalink_original'],
            ] + $meta_datas_to_import);
            $media->save();
          }
          else {

            return 2;
          }
        }
        else {
          return 4;
        }

      }
      else {

        return 3;
      }
    }

    return 1;
  }

  /**
   * Filter metadatas values.
   *
   * @param array $meta_datas
   *   Meta datas to filter.
   * @param array $all_filters
   *   Dynamic filter to use.
   */
  public function filterMetadatas(array $meta_datas, array $all_filters) {
    $meta_datas_to_import = [];
    $meta_datas_cleaned = [];
    if (isset($meta_datas[self::EMBEDDED_KEY]['metadata'])) {
      $mapping = [];
      foreach ($all_filters as $filter) {
        [$value, $key] = explode('|', $filter);
        $mapping[$key] = $value;
      }
      foreach ($meta_datas[self::EMBEDDED_KEY]['metadata'] as $metadata) {
        $meta_datas_cleaned[$metadata['id']] = $metadata;
      }
      foreach ($mapping as $drupal_field => $fid) {
        if (isset($meta_datas_cleaned[$fid])) {
          $meta_datas_to_import[$drupal_field] = $this->getMetadataValue($meta_datas_cleaned[$fid]);
        }
      }
    }

    return $meta_datas_to_import;
  }

  /**
   * Get Metadata value.
   *
   * @param array $metadata
   *   The metadata array.
   *
   * @return mixed|string
   *   The result.
   */
  public function getMetadataValue(array $metadata) {
    $value = '';
    if (isset($metadata['value'])) {
      $value = $metadata['value'];
    }
    elseif (isset($metadata[self::EMBEDDED_KEY]['value'])) {
      $terms = [];

      // Case with only one value.
      if (isset($metadata[self::EMBEDDED_KEY]['value']['title'])) {
        $terms[] = $metadata['_embedded']['value']['title'];
      }
      // Case with multi values.
      else {
        foreach ($metadata[self::EMBEDDED_KEY]['value'] as $term) {
          if (isset($term['title'])) {
            $terms[] = $term['title'];
          }
        }
      }

      $value = implode(', ', $terms);
    }

    return $value;
  }

  /**
   * Check if a Keepeek media is already imported.
   *
   * @param int $id
   *   Id of media to check.
   *
   * @return bool
   *   The result.
   */
  public function checkExistMediaById(int $id) {
    $query = $this->entityTypeManager->getStorage('media')->getQuery();
    $media_query = $query->condition('field_keepeek_id', $id)
      ->accessCheck(TRUE);
    $execution = $media_query->execute();

    return (reset($execution) ?: FALSE);
  }

  /**
   * Get the map values.
   *
   * @param string|null $type
   *   Type te retrive.
   * @param bool $key
   *   Search in keys.
   *
   * @return array|mixed|string|string[]
   *   The value returned.
   */
  public function getMap(string $type = NULL, bool $key = FALSE) {

    if ($type) {
      if ($key) {
        return self::MAP[$type];
      }

      return array_search($type, self::MAP);
    }

    return self::MAP;
  }

  /**
   * Check if Keepeek module is configured.
   *
   * @return bool
   *   Status of configuration.
   */
  public function checkConfiguration() {
    if ($this->config->get('username') && $this->config->get('password')) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get the public share link of a keepeek media if exist.
   *
   * @param int $id
   *   The Keepeek media ID.
   *
   * @return mixed|string
   *   Url of video player.
   */
  public function getPublicShareLink(int $id) {
    $response = $this->runQuery('GET', self::METHOD_SAVE_LAST_QUERY['medias'] . $id . '/share/public');
    $return = NULL;
    if ($response && $response->getStatusCode() == Response::HTTP_OK) {
      $data = Json::decode($response->getBody());
      if (isset($data[self::LINKS_KEY][self::PUBLIC_SHARE_LINK])) {
        $return = $data[self::LINKS_KEY][self::PUBLIC_SHARE_LINK]['href'];
      }
      // If is not shared yet, so publish it.
      else {
        $request_options = [];
        $request_options[RequestOptions::HEADERS]['Accept'] = 'application/json';
        $request_options[RequestOptions::HEADERS]['Content-Type'] = 'application/json';
        $request_options[RequestOptions::BODY] = '{"status": "ACTIVATED","accessCode": "WebFactory"}';
        $response = $this->runQuery('PUT', self::METHOD_SAVE_LAST_QUERY['medias'] . $id . '/share/public', $request_options);
        if ($response && $response->getStatusCode() == Response::HTTP_NO_CONTENT) {
          return $this->getPublicShareLink($id);
        }
      }
    }

    return $return;
  }

  /**
   * Check if media is ready for publication.
   *
   * @param int $id
   *   Id of media selected.
   */
  public function isReadyForPublication(int $id) {
    $response = $this->runQuery('GET', self::METHOD_SAVE_LAST_QUERY['medias'] . $id);
    if ($response && $response->getStatusCode() == Response::HTTP_OK) {
      $return = Json::decode($response->getBody());
      if (isset($return['_links']['kpk:custom-status']['href'])) {
        if (strpos($return['_links']['kpk:custom-status']['href'], self::METHOD_SAVE_LAST_QUERY['path_status'] . $this->config->get('id_ready_for_publication_status')) !== FALSE) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Change status on Keepeek from "Ready for publication" to "Published".
   *
   * @param int $id
   *   The Keepeek media ID.
   */
  public function publishAsset(int $id) {
    $request_options = [];
    $request_options[RequestOptions::HEADERS]['Accept'] = 'application/json';
    $request_options[RequestOptions::HEADERS]['Content-Type'] = 'application/json';
    $request_options[RequestOptions::BODY] = '{"status":"PUBLISHED","kpk:custom-status":"' . $this->config->get('base_url') . '/dam/' . self::METHOD_SAVE_LAST_QUERY['path_status'] . $this->config->get('id_published_status') . '"}';
    if ($this->isReadyForPublication($id)) {
      $response = $this->runQuery('PUT', self::METHOD_SAVE_LAST_QUERY['publish'] . $id, $request_options);
      if ($response && $response->getStatusCode() == Response::HTTP_NO_CONTENT) {
        return '1';
      }
      return '2';
    }
    return '3';

  }

  /**
   * Get media IDs out of a given entity's fields.
   *
   * @param string $entity_type_id
   *   A given entity type id.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   A given entity.
   *
   * @return array
   *   The list of entity IDs found in fields.
   */
  public function extractEntityIdsFromEntity(string $entity_type_id, EntityInterface $entity) {
    $ids = [];
    foreach ($entity->getFieldDefinitions() as $field_name => $field_definition) {
      switch ($field_definition->getType()) {
        case 'image':
          foreach ($entity->get($field_name)->referencedEntities() as $reference) {
            if (strpos($reference->bundle(), 'keepeek') !== FALSE) {
              if ($reference->hasField('field_keepeek_id')) {
                $ids[] = $reference->get('field_keepeek_id')->getString();
              }
            }
          }
          break;

        case 'entity_reference':
          if (($field_definition->getSettings()['target_type'] ?? NULL) == $entity_type_id) {
            foreach ($entity->get($field_name)->referencedEntities() as $reference) {
              if (strpos($reference->bundle(), '_component') !== FALSE) {
                $ids[] = $reference->id();
              }
              elseif (strpos($reference->bundle(), 'keepeek') !== FALSE) {
                if ($reference->hasField('field_keepeek_id')) {
                  $ids[] = $reference->get('field_keepeek_id')->getString();
                }
              }
            }
          }
          break;

        case 'entity_reference_revisions':
          foreach ($entity->get($field_name)->referencedEntities() as $paragraph) {
            // Recursively call.
            $ids = array_merge($ids, $this->extractEntityIdsFromEntity($entity_type_id, $paragraph));
          }
          break;

        case 'text':
        case 'text_long':
        case 'text_with_summary':
          $ids = array_merge($ids, $this->extractEntityIdsFromText($entity_type_id, $entity->get($field_name)->getString()));
          break;
      }
    }

    return $ids;
  }

  /**
   * Parse textfields to get <data-entity-XXX> attributes.
   *
   * @param string $entity_type_id
   *   A given entity type to look for.
   * @param string $text
   *   A given string, with HTML por favor.
   *
   * @return array
   *   The list of entity IDs found.
   *
   * @see \Drupal\media\Plugin\Filter\MediaEmbed::process();
   */
  public function extractEntityIdsFromText(string $entity_type_id, string $text) {
    $ids   = [];
    $dom   = Html::load($text);
    $xpath = new \DOMXPath($dom);
    $query = $this->entityTypeManager->getStorage($entity_type_id)->getQuery();
    $query->condition('bundle', 'keepeek', 'CONTAINS')->accessCheck(TRUE);

    if ($results = $xpath->query('//*[@data-entity-type="' . $entity_type_id . '" and normalize-space(@data-entity-uuid)!=""]')) {
      foreach ($results as $node) {
        $q = clone $query;
        $uuid = $node->getAttribute('data-entity-uuid');
        $mids = $q->condition('uuid', $uuid)->accessCheck(TRUE)->execute();
        $medias = $this->entityTypeManager->getStorage('media')->loadMultiple($mids);
        foreach ($medias as $media) {
          $ids[] = $media->get('field_keepeek_id')->getString();
        }
      }
    }

    return $ids;
  }

  /**
   * Get list of blocks content for a node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   A given entity.
   */
  public function getBlocksContentByNode(EntityInterface $entity) {
    $block_content_ids = [];
    if (!$entity instanceof NodeInterface) {
      return [];
    }
    if (!$entity->hasField('layout_builder__layout') || $entity->get('layout_builder__layout')->isEmpty()) {
      return [];
    }
    $layout = $entity->get('layout_builder__layout')->getValue();
    foreach ($layout as $item) {
      $section = $item['section'];
      $section_array = $section->toArray();
      foreach ($section_array['components'] as $component) {
        if (isset($component['configuration']['block_revision_id'])) {
          $id = $component['configuration']['block_revision_id'];
          if (in_array($id, $block_content_ids)) {
            continue;
          }
          $block_content_ids[] = $id;
        }
      }
    }

    return $block_content_ids;
  }

  /**
   * Get All imported Keepeek Assets filtred by type.
   *
   * @param string $type
   *   Type to filter with.
   *
   * @return array|int
   *   List of media ids.
   */
  public function getImportedKeepeekAssets(string $type = 'keepeek') {
    $query = $this->entityTypeManager->getStorage('media')->getQuery();
    $query->condition('bundle', $type, 'CONTAINS')->accessCheck(TRUE);

    return $query->execute();
  }

  /**
   * Update Permalink generic path extension for a Keepeek image.
   *
   * @param string $id
   *   The media id.
   * @param string $from
   *   Extension to modify.
   * @param string $to
   *   Extension to replace.
   */
  public function updatePermalinkGenericPathExtensionKeepeekImage(string $id, string $from, string $to) {
    $need_update = FALSE;
    $media = $this->entityTypeManager->getStorage('media')->load($id);
    if ($media instanceof MediaInterface) {
      $json = $media->get('field_media_keepeek_image')->getString();
      $data = Json::decode($json);
      if (isset($data[self::EMBEDDED_KEY]['metadata'])) {
        foreach ($data[self::EMBEDDED_KEY]['metadata'] as &$field) {
          if ($field['id'] == 'permalink_generic_path') {
            if (strpos($field['value'], '.' . $from) !== FALSE) {
              $field['value'] = str_replace('.' . $from, '.' . $to, $field['value']);
              $need_update = TRUE;
              break;
            }
          }
        }
      }
      if ($need_update) {
        $media->set('field_media_keepeek_image', Json::encode($data));
        return $media->save();
      }

      return FALSE;
    }
  }

}
