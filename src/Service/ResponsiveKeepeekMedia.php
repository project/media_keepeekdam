<?php

namespace Drupal\media_keepeekdam\Service;

use Drupal\breakpoint\BreakpointInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Template\Attribute;
use Drupal\responsive_image\ResponsiveImageStyleInterface;

/**
 * Responsive Keepeek Media Service.
 */
class ResponsiveKeepeekMedia {

  // @todo To be made configurable.
  protected const GENERIC_PATH_FIELD = 'permalink_generic_path';
  protected const DEFAULT_STYLE_GENERIC_PATH = 'wvh';

  /**
   * A config object to retrieve Keepeek DAM config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Entity Type Manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityStorage;

  /**
   * The image factory service.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The Keepeek Media service.
   *
   * @var \Drupal\media_keepeekdam\Service\KeepeekMedia
   */
  protected $mediaService;

  /**
   * ResponsiveKeepeekMedia constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config object to retrieve Keepeek DAM auth information from.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity query service.
   * @param \Drupal\media_keepeekdam\Service\KeepeekMedia $media_service
   *   The Keepeek Media service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, KeepeekMedia $media_service, ImageFactory $image_factory = NULL) {
    $this->config = $config_factory->get('media_keepeekdam.settings');
    $this->entityStorage = $entity_type_manager;
    $this->imageFactory = $image_factory;
    $this->mediaService = $media_service;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponsiveImageStylesOptions() {
    $options = [];
    $query = $this->entityStorage->getStorage('responsive_image_style')->getQuery();
    $responsive_image_style_ids = $query->execute();
    $responsive_image_styles = $this->entityStorage->getStorage('responsive_image_style')->loadMultiple($responsive_image_style_ids);
    foreach ($responsive_image_styles as $responsive_image_style) {
      $options[$responsive_image_style->id()] = $responsive_image_style->label();
    }

    return $options;
  }

  /**
   * Wrapper around image_style_url() so we can return an empty image.
   */
  public function responsiveKeepeekImageStyleUrl($style_name, $variables) {
    if ($style_name == ResponsiveImageStyleInterface::EMPTY_IMAGE) {
      // The smallest data URI for a 1px square transparent GIF image.
      // http://probablyprogramming.com/2009/03/15/the-tiniest-gif-ever
      return 'data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
    }
    else {
      $uri = $this->buildKeepeekImageStyle($variables['mid'], $style_name);
    }

    return ($uri ?: $variables['uri']);
  }

  /**
   * Build Source Attributes For Responsive Keepeek Image.
   *
   * @param array $variables
   *   Variables.
   * @param \Drupal\breakpoint\BreakpointInterface $breakpoint
   *   Breakpoint.
   * @param array $multipliers
   *   Multipliers.
   *
   * @return \Drupal\Core\Template\Attribute
   *   The attribute.
   */
  public function responsiveKeepeekImageBuildSourceAttributes(array $variables, BreakpointInterface $breakpoint, array $multipliers) {
    $extension = pathinfo($variables['uri'], PATHINFO_EXTENSION);
    $srcset = [];
    $mapping = $this->getImageStylesMapping();
    foreach ($multipliers as $multiplier => $image_style_mapping) {
      $keepeek_style = $mapping[$image_style_mapping['image_mapping']];
      $srcset[intval(mb_substr($multiplier, 0, -1) * 100)] = $this->responsiveKeepeekImageStyleUrl($keepeek_style, $variables) . ' ' . $multiplier;
    }
    // Sort the srcset from small to large image width or multiplier.
    ksort($srcset);
    $source_attributes = new Attribute([
      'srcset' => implode(', ', array_unique($srcset)),
    ]);
    $media_query = trim($breakpoint->getMediaQuery());
    if (!empty($media_query)) {
      $source_attributes->setAttribute('media', $media_query);
    }

    // Get Supported Image Core extensions.
    $extensions = $this->imageFactory->getSupportedExtensions();
    $expected_image_types = array_combine(
      $extensions,
      array_map(function ($value) {
        if (in_array($value, ['jpg', 'jpeg', 'jpe'])) {
          $value = 'jpeg';
        }
        return constant('IMAGETYPE_' . strtoupper($value));
      }, $extensions)
    );

    // @todo maybe to be removed when migrate to Core 9.2.
    if (!in_array('webp', $expected_image_types)) {
      $expected_image_types['webp'] = IMAGETYPE_WEBP;
    }

    $mime_type = image_type_to_mime_type($expected_image_types[strtolower($extension)]);

    if ($mime_type) {
      $source_attributes->setAttribute('type', $mime_type);
    }

    return $source_attributes;
  }

  /**
   * Get Image Styles Mapping.
   *
   * @return array
   *   The mapping between image styles Drupal & Keepeek.
   */
  public function getImageStylesMapping() {
    $mapping = [];
    foreach ($this->config->get('image_styles_mapping') as $map) {
      [$key, $value1, $value2] = explode('|', $map);
      $mapping[$key] = [$value1, $value2];
    }

    return $mapping;
  }

  /**
   * Build Keepeek Image Style.
   *
   * @return string
   *   The uri of the style.
   */
  public function buildKeepeekImageStyle($mid, $style) {
    $uri = '';
    $keepeek_image = $this->entityStorage->getStorage('media')->load($mid);
    $original_json = Json::decode($keepeek_image->get('field_media_keepeek_image')->getString());
    foreach ($original_json['_embedded']['metadata'] as $metadata) {
      if ($metadata['id'] == self::GENERIC_PATH_FIELD) {
        // Example : https://assets.site.com/5407/uhd/products-2019.jpg
        // The uhd part represent the machine name of image style
        // on the keepeek level.
        $uri = $metadata['value'];
        break;
      }
    }
    if (!$uri && isset($original_json['_links'][$style[0]]['href'])) {
      $uri = $original_json['_links'][$style[0]]['href'];
    }

    // Replace the domain with the CDN Keepeek if defined.
    return $this->mediaService->applyCdnKeepeek($uri, 'keepeek_image', $style, self::DEFAULT_STYLE_GENERIC_PATH);
  }

}
