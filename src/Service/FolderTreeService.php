<?php

namespace Drupal\media_keepeekdam\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * API Folder Tree.
 */
class FolderTreeService extends ClientFactory {

  /**
   * The current logged user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The cache factory service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Client constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current User.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, AccountProxyInterface $current_user = NULL, CacheBackendInterface $cache) {
    parent::__construct($config_factory, $logger_factory);
    $this->currentUser = $current_user;
    $this->cache = $cache;
  }

  /**
   * APIFolderTree: recover all the structure of the Keepeekdam folders.
   *
   * @param string $type
   *   Contain type of folder 'Child' or 'parent'.
   * @param int $idFolder
   *   Id of parent folder.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getTreeFolder($type = NULL, $idFolder = NULL) {
    try {
      $folders = NULL;
      $link = 'folder-tree';
      $cid = 'media_keepeekdam:parent_folder';
      if ($type == 'child') {
        $cid = 'media_keepeekdam:child_folder';
        $link = 'folder-tree/' . $idFolder;
      }
      // Check if the cache contains the values.
      $cache = $this->cache->get($cid);
      if (!empty($cache)) {
        // Get child folder by ID.
        if ($type == 'child' && !empty($cache->data[$idFolder])) {
          $folders = $cache->data[$idFolder];
        }
        // Get parent folder by ID.
        if ($type == 'parent') {
          $folders = $cache->data;
        }
      }
      if (empty($folders)) {
        $response = $this->runQuery('GET', $link);
        if ($response) {
          if ($response->getStatusCode() == Response::HTTP_OK) {
            $folders = Json::decode($response->getBody());
            if ($type == 'child') {
              // Set Cache with the child folder.
              $cache_data[$idFolder] = $folders;
              $this->cache->set($cid, $cache_data);
            }
            else {
              // Set Cache with the parent folder.
              $this->cache->set($cid, $folders);
            }
          }
        }
      }
      return $folders;
    }
    catch (\Exception $e) {
      $current_url = Url::fromRoute('<current>', [], ["absolute" => TRUE])->toString();
      $error_message = $e->getMessage();
      $this->setError($error_message, $current_url);
      return ['error' => $error_message, 'status' => $e->getCode()];
    }
  }

  /**
   * Delete Cache.
   *
   * @param string $cid
   *   Cid of cache.
   */
  public function deleteCache($cid) {
    $this->cache->delete($cid);
  }

}
