<?php

namespace Drupal\media_keepeekdam\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\media_keepeekdam\Plugin\media\Source\KeepeekMedia;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Class to manage connexion to the Keepeek DAM API.
 */
class ClientFactory {

  protected const PREFIX_LINK = '/api/dam/';

  /**
   * A config object to retrieve Keepeek DAM auth information from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * A fully-configured Guzzle client to pass to the dam client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $guzzleClient;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerFactory;

  /**
   * ClientFactory constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config object to retrieve Keepeek DAM auth information from.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The Logger Factory Service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->config = $config_factory->get('media_keepeekdam.settings');
    $this->loggerFactory = $loggerChannelFactory->get('media_keepeekdam');
    $params = [
      'base_url' => $this->config->get('base_url'),
      'username' => $this->config->get('username'),
      'password' => $this->config->get('password'),
      'proxy_status' => $this->config->get('proxy_status'),
      'proxy_url' => $this->config->get('proxy_url'),
    ];
    $this->guzzleClient = $this->getClient($params);

  }

  /**
   * Get client object by params.
   *
   * @param array $params
   *   A collection of params to connect to Keepeek API.
   *
   * @return \GuzzleHttp\Client
   *   Guzzle HTTP client.
   */
  public function getClient(array $params) {
    if (empty($params)) {
      return $this->guzzleClient;
    }
    $configs = [
      'base_uri' => $params['base_url'],
      'timeout' => $this->config->get('curl_timeout'),
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode($params['username'] . ':' . $params['password']),
      ],
    ];

    if ($params['proxy_status']) {

      $configs['proxy'] = [
        'http' => $params['proxy_url'],
        'https' => $params['proxy_url'],
      ];
      $configs['verify'] = FALSE;
    }

    return new Client($configs);
  }

  /**
   * A query runner to request interface.
   *
   * @param string $method
   *   HTTP method to use.
   * @param string $path
   *   Path of inteface to request.
   * @param array $params
   *   List of params.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   A HTTP response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function runQuery(string $method = 'GET', string $path = '/search/media', array $params = []) {

    try {
      return $this->guzzleClient->request($method, self::PREFIX_LINK . $path, $params);
    }
    catch (ClientException $e) {
      $status_code = $e->getResponse()->getStatusCode();
      $this->loggerFactory->error(
        'Unable to access. Keepeek DAM API client returned a @code exception code with the following message: @message',
        [
          '@code' => $status_code,
          '@message' => $e->getMessage(),
        ]
      );
    }
  }

  /**
   * Setter Error.
   *
   * @param mixed $errors
   *   Error.
   * @param mixed $source
   *   Source.
   */
  public function setError($errors, $source = NULL) {

    if (PHP_SAPI === 'cli') {
      $this->loggerFactory->error('API error: @error', [
        '@error' => $errors,
      ]);
    }
    else {
      $this->loggerFactory->error('API error in @source: @error', [
        '@soucre' => $source,
        '@error' => $errors,
      ]);
    }
  }

  /**
   * Parse json data.
   *
   * @param string $json
   *   The json data to be parsed.
   * @param array $attributes
   *   The list of attributes.
   *
   * @return mixed
   *   The parsed result.
   */
  public function parseData(string $json = '{}', array $attributes = []) {
    $style = KeepeekMedia::METADATA_ATTRIBUTE_THUMBNAIL_STYLE;
    $parsed = [];
    $arrayData = json_decode($json);
    if (!empty($arrayData)) {
      foreach ($arrayData as $key => $data) {
        if (array_key_exists($key, $attributes)) {
          $parsed[$key] = $data;
        }
        else {
          if (is_object($data) && isset($data->$style)) {
            $parsed[$style] = $data->$style->href;
          }
        }
      }
    }

    return $parsed;
  }

}
