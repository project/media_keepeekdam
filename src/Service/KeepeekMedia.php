<?php

namespace Drupal\media_keepeekdam\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Keepeek Media Service.
 */
class KeepeekMedia {

  /**
   * A config object to retrieve Keepeek DAM config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * ResponsiveKeepeekMedia constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config object to retrieve Keepeek DAM auth information from.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('media_keepeekdam.settings');
  }

  /**
   * Apply the CDN Keepeek if defined.
   *
   * @param string $uri
   *   The URI.
   * @param string $bundle
   *   The Bundle.
   * @param array $replace_style
   *   The replace style.
   * @param string $search_style
   *   The search style.
   *
   * @return string|string[]
   *   The URI modified.
   */
  public function applyCdnKeepeek(string $uri, string $bundle, array $replace_style = [], string $search_style = '') {
    // Defaut CDN domain for Image and Document media.
    $config_cdn = 'cdn_keepeek';
    $replace = NULL;
    // Change CDN domain for Video media.
    if ($bundle == 'keepeek_video') {
      $config_cdn = 'cdn_keepeek_video_player';
    }
    if (isset($replace_style[1])) {
      $replace = $replace_style[1];
    }
    elseif (isset($replace_style[0])) {
      $replace = $replace_style[0];
    }
    if ($replace) {
      $uri = str_replace($search_style, $replace, $uri);
    }
    // If CDN Keepeek configured.
    if ($this->config->get($config_cdn)) {
      $uri = parse_url($uri);
      if (!isset($uri['scheme'])) {
        $uri['scheme'] = 'https';
      }
      $uri['host'] = $this->config->get($config_cdn);
      $uri = $this->buildUrl($uri);
    }

    return $uri;
  }

  /**
   * Build URL from parts.
   *
   * @param array $parts
   *   The parts of URL.
   *
   * @return string
   *   The URL builded.
   */
  public function buildUrl(array $parts) {
    $scheme   = isset($parts['scheme']) ? ($parts['scheme'] . '://') : '';
    $host     = $parts['host'] ?? '';
    $port     = isset($parts['port']) ? (':' . $parts['port']) : '';
    $user     = $parts['user'] ?? '';
    $pass     = isset($parts['pass']) ? (':' . $parts['pass']) : '';
    $pass     = ($user || $pass) ? ($pass . '@') : '';
    $path     = $parts['path'] ?? '';
    $query    = empty($parts['query']) ? '' : ('?' . $parts['query']);
    $fragment = empty($parts['fragment']) ? '' : ('#' . $parts['fragment']);

    return implode('', [
      $scheme,
      $user,
      $pass,
      $host,
      $port,
      $path,
      $query,
      $fragment,
    ]);
  }

}
