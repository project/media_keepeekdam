<?php

namespace Drupal\media_keepeekdam\Service;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Class to Check Access to the Keepeek module.
 */
class AccessCheck {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Constructs a new AccessCheck instance.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(AccountInterface $account) {
    $this->account = $account;
  }

  /**
   * Check Access to Keepeek media.
   *
   * @param string $type
   *   The type.
   * @param string $operation
   *   The operation.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral
   *   The result.
   */
  public function keepeekImageMediaAccess(string $type, string $operation) {
    $permission = $operation . ' keepeek ' . $type . ' media';
    if ($operation === 'view') {
      // If the operation is 'view' then don't alter the access.
      return AccessResult::neutral();
    }
    return AccessResult::allowedIfHasPermissions($this->account, [$permission], 'OR');
  }

}
