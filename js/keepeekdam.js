(function ($, Drupal, drupalSettings) {
  // get URL Page.
  var sourceURl = window.location.href;
  Drupal.behaviors.Keepeekdam = {
    attach: function (context, settings) {
        // Counter, this variable to execute the scroll only once.
        var counter = 0

        if(urlParam('id') != null){
          if(counter == 0){
            var element = $('#' + urlParam('id'));
            $([document.documentElement, document.body]).animate({
              scrollTop: $(element).offset().top
            }, 400);
            counter++;
          }
        }
      $('.folder', context).click(function (e) {
        sourceURl = removeParam('id',sourceURl);
      });

      // Check url contain "#popup" to keep pop-up open.
      if(checkUrl('#popup') == true ){
        $("#myModalFolder").css('display','block');
      }
    }
  }
  // Check url contains popup.
  var checkUrl = function (name){
    if (sourceURl.indexOf(name) > -1){
      return true;
    }
    return false;
  }
  // This function to check url contains the "id" param.
  var urlParam =  function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(sourceURl);
    if (results == null) {
      return null;
    }
    return decodeURI(results[1]) || 0;
  }
  // This function to remove param in url.
  var removeParam = function(key,sourceURL) {
    var rtn = sourceURL.split("?")[0],
      param,
      params_arr = [],
      queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
      params_arr = queryString.split("&");
      for (var i = params_arr.length - 1; i >= 0; i -= 1) {
        param = params_arr[i].split("=")[0];
        if (param === key) {
          params_arr.splice(i, 1);
        }
      }
      if (params_arr.length) rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
  }
}(jQuery, Drupal, drupalSettings));

