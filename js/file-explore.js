(function ($) {
  $.fn.filetree = function (method) {
    var settings = {
      // settings to expose
      animationSpeed: "fast",
      collapsed: true,
      console: true,
    };
    var methods = {
      init: function (options) {
        // Get standard settings and merge with passed in values
        var options = $.extend(settings, options);
        // Do this for every file tree found in the document
        return this.each(function () {
          var $fileList = $(this);
          var hasOpenClass = $fileList
            .addClass("file-list")
            .find("li")
            .has("ul")
            .hasClass("open");
          if (!hasOpenClass) {
            $fileList
              .addClass("file-list")
              .find("li")
              .has("ul") // Any li that has a list inside is a folder root
              .addClass("folder-root closed")
              .on("click", ".collapse-folder", function (e) {
                // Add a click override for the folder root links
                e.preventDefault();
                $(this).parent().toggleClass("closed").toggleClass("open");
                var has_child = $(this).attr("data-has-child");
                if (has_child == "1" || has_child == "true") {
                  $(this).toggleClass("minus").toggleClass("plus");
                }
                return false;
              });
            // $fileList.find('li').hasClass('open').addClass('closed').removeClass('open');
          } else {
            $fileList
              .addClass("file-list")
              .find("li")
              .has("ul") // Any li that has a list inside is a folder root
              .on("click", ".collapse-folder", function (e) {
                // Add a click override for the folder root links
                e.preventDefault();
                $(this).parent().toggleClass("closed").toggleClass("open");
                var has_child = $(this).attr("data-has-child");
                if (has_child == "1" || has_child == "true") {
                  $(this).toggleClass("minus").toggleClass("plus");
                }

                return false;
              });
          }
        });
      },
    };

    if (typeof method === "object" || !method) {
      return methods.init.apply(this, arguments);
    } else {
      $.on("error", function () {});
    }
  };
})(jQuery);
