(function ($, Drupal, drupalSettings) {
  $(".file-tree").filetree();
  Drupal.behaviors.getChildFolder = {
    attach: function (context, settings) {
      // Add Modal.
      // When the user clicks on the button, open the modal
      $("#select-folder").click(function(e){
        $("#myModalFolder").css('display','block');
      });
      // When the user clicks on the button, close the modal.
      $(".close").click(function(e){
        $("#myModalFolder").css('display','none');
        // Check URL contain #popup.
        if (location.href.indexOf('#popup') > -1){
          // Delete #popup in URL.
          var newURL = location.href.split("#popup")[0];
          window.history.pushState('object', document.title, newURL);
        }
      })
      // End Modal.
      if($('input[data-drupal-selector="edit-folder-name"]').val()){
        if($('#nameFolder').length){
          $('#nameFolder').remove();
        }
        $('#edit-filters .details-wrapper').prepend('<label id="nameFolder">Folder : '+
          $('input[data-drupal-selector="edit-folder-name"]').val() +'</label>');
      }
      $('.folder', context).click(function (e) {
         var id_folder = $(this).attr('data-id-folder');
         var has_child = $(this).attr('data-has-child');
         var title = $(this).attr('data-title');
         var clicked = $(this).hasClass('clicked');
         if($('#nameFolder').length){
           $('#nameFolder').remove();
         }
         $('input[data-drupal-selector="edit-folder-id"]').val(id_folder);
         var name_folder = title + ' (folder ID: ' +id_folder +')';
         $('input[data-drupal-selector="edit-folder-name"]').val(name_folder);
         var remove_selected_btn = '<span class="remove-selected" id="remove-selected">X</span>';
         $('#remove-selected').remove();
         $('#edit-filters .details-wrapper').prepend('<label id="nameFolder">Folder : '+ name_folder +'</label>' + remove_selected_btn);
         $("#myModalFolder").css('display','none');
        // Add event remove selection
          $('.remove-selected').click(function(e){
            $('#nameFolder').remove();
            $('.remove-selected').remove();
            $('#edit-filters .details-wrapper').prepend('<label id="nameFolder">Folder : No selection</label>' );

          });
      });
      $('.collapse-folder', context).click(function (e) {
        var id_folder = $(this).attr('data-id-folder');
        var has_child = $(this).attr('data-has-child');
        var title = $(this).attr('data-title');
        var clicked = $(this).hasClass('clicked');
        if((has_child == "1" || has_child == "true") && clicked == false){
          $('a[data-id-folder="'+id_folder+'"]').next('ul').append('<label id="loading">' + Drupal.t("loading...") + '</label>');
          $.ajax({
            type: 'GET',
            url: Drupal.url('get-child-folder/' + id_folder),
            success: function (result) {
              $('span[data-id-folder="'+id_folder+'"]').addClass('clicked');
              $('#loading').remove();
              var newContent = $('a[data-id-folder="'+id_folder+'"]').next('ul').append(result);
              Drupal.attachBehaviors(newContent[0]);
              if(newContent){
                var thHtml = $('#edit-folders').eq(0).html();
                saveInCache.set(thHtml);
              }

            }
          });
        }
      });
      $('.purge-session', context).click(function (e) {
        $.ajax({
          type: 'GET',
          url: Drupal.url('purge-session'),
          success: function (result) {
            // After reset folder tree add #popup in url.
            var url = window.location.href;
            if (url.indexOf('#popup') == -1){
              window.location.replace(url + "#popup")
            }
            $('#edit-purge-session').prop( "disabled", true );
            window.location.reload();
          }
        });
      });

    }
  }
  var saveInCache = {
    set: function (cachedData) {
      $.ajax({
        type: 'POST',
        data: { content: cachedData},
        url: Drupal.url('save-html-cache'),
        success: function (response) {
        }
      });
    }
  };
}(jQuery, Drupal, drupalSettings));

