<?php

namespace Drupal\Tests\media_keepeekdam\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Form\FormState;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\State\State;
use Drupal\media_keepeekdam\Service\ClientFactory;
use Drupal\media_keepeekdam\Form\KeepeekdamConfig;
use Drupal\Tests\media_keepeekdam\Traits\KeepeekdamConfigTrait;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;

/**
 * Config form test.
 *
 * @coversDefaultClass \Drupal\media_keepeekdam\Form\KeepeekdamConfig
 *
 * @group media_keepeekdam
 */
class KeepeekdamConfigFormTest extends UnitTestCase {

  use KeepeekdamConfigTrait;

  /**
   * Container builder helper.
   *
   * @var \Drupal\Core\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * Media: Keepeek DAM config form.
   *
   * @var \Drupal\Tests\media_keepeekdam\Unit\KeepeekdamConfig
   */
  protected $keepeekDamConfig;

  /**
   * Drupal State service.
   *
   * @var \Drupal\Core\State\State|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $state;

  /**
   * {@inheritdoc}
   *
   * @covers ::getFormId
   */
  public function testGetFormId() {
    $this->assertEquals('keepeekdam_config',
      $this->keepeekDamConfig->getFormId());
  }

  /**
   * {@inheritdoc}
   *
   * @covers ::buildForm
   */
  public function testBuildForm() {
    $form = $this->keepeekDamConfig->buildForm([], new FormState());
    $this->assertArrayHasKey('authentication', $form);
    $this->assertArrayHasKey('base_url', $form['authentication']);
    $this->assertArrayHasKey('username', $form['authentication']);
    $this->assertArrayHasKey('password', $form['authentication']);
    $this->assertArrayHasKey('curl_timeout', $form['authentication']);

    $this->assertEquals('https://keepeek.com/api',
      $form['authentication']['base_url']['#default_value']);
    $this->assertEquals('KPKusername',
      $form['authentication']['username']['#default_value']);
    $this->assertEquals('KPKpassword',
      $form['authentication']['password']['#default_value']);
    $this->assertEquals(20,
      $form['authentication']['curl_timeout']['#default_value']);

    $this->assertArrayHasKey('api_config', $form);
    $this->assertArrayHasKey('common_media_metadatas', $form['api_config']);
    $this->assertArrayHasKey('image_media_metadatas', $form['api_config']);
    $this->assertArrayHasKey('video_media_metadatas', $form['api_config']);
    $this->assertArrayHasKey('document_media_metadatas', $form['api_config']);
    $this->assertArrayHasKey('image_styles_mapping', $form['api_config']);
    $this->assertArrayHasKey('cdn_keepeek', $form['api_config']);
    $this->assertArrayHasKey('cdn_keepeek_video_player', $form['api_config']);

    $this->assertEquals('descriptor__keywords|field_description, title|field_alt',
      $form['api_config']['common_media_metadatas']['#default_value']);
    $this->assertEquals('caption|field_title',
      $form['api_config']['image_media_metadatas']['#default_value']);
    $this->assertEquals('caption|field_title',
      $form['api_config']['video_media_metadatas']['#default_value']);
    $this->assertEquals('caption|field_title',
      $form['api_config']['document_media_metadatas']['#default_value']);
    $this->assertEquals('style_1|kpk:style1|wvh',
      $form['api_config']['image_styles_mapping']['#default_value']);
    $this->assertEquals('https://cdn.keepeek.com/',
      $form['api_config']['cdn_keepeek']['#default_value']);
    $this->assertEquals('https://cdn.keepeek.com/video',
      $form['api_config']['cdn_keepeek_video_player']['#default_value']);

    $this->assertArrayHasKey('ui_config', $form);
    $this->assertArrayHasKey('results_per_page', $form['ui_config']);
    $this->assertArrayHasKey('global_name', $form['ui_config']);
    $this->assertArrayHasKey('id_published_status', $form['api_config']);
    $this->assertArrayHasKey('id_ready_for_publication_status', $form['api_config']);

    $this->assertEquals(20,
      $form['ui_config']['results_per_page']['#default_value']);
    $this->assertEquals('Keepeek DAM',
      $form['ui_config']['global_name']['#default_value']);
    $this->assertEquals('1',
      $form['api_config']['id_published_status']['#default_value']);
    $this->assertEquals('2',
      $form['api_config']['id_ready_for_publication_status']['#default_value']);

    $this->assertArrayHasKey('cron_config', $form);
    $this->assertArrayHasKey('refresh_interval', $form['cron_config']);

    $this->assertEquals(-1,
      $form['cron_config']['refresh_interval']['#default_value']);

    $this->assertArrayHasKey('proxy', $form);
    $this->assertArrayHasKey('proxy_status', $form['proxy']);
    $this->assertArrayHasKey('proxy_url', $form['proxy']);

    $this->assertEquals(1,
      $form['proxy']['proxy_status']['#default_value']);
    $this->assertEquals('http://proxy.com:8080',
      $form['proxy']['proxy_url']['#default_value']);
  }

  /**
   * Get a partially mocked KeepeekdamConfig object.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject|\Drupal\media_keepeekdam\Form\KeepeekdamConfig
   *   A mocked version of the KeepeekdamConfig form class.
   *
   * @throws \Exception
   */
  protected function getMockedKeepeekdamConfig() {

    $messenger = $this->createMock(Messenger::class);

    $config = $this->getMockBuilder(KeepeekdamConfig::class)
      ->setConstructorArgs([
        $this->container->get('config.factory'),
        $this->container->get('messenger'),
        $this->container->get('media_keepeekdam.client'),
      ])
      ->getMock();

    $config->method('messenger')->willReturn($messenger);

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // We need to override the DAM client so that we can fake authentication.
    $dam_client = $this->createMock(Client::class);

    $messenger = $this->createMock(Messenger::class);

    // We need to make sure we get our mocked class instead of the original.
    $keepeekdam_client_factory = $this->createMock(ClientFactory::class);
    $keepeekdam_client_factory->expects($this->any())
      ->method('getClient')
      ->willReturn($dam_client);

    $this->state = $this->createMock(State::class);

    $this->container = new ContainerBuilder();
    $this->container->set('config.factory', $this->getConfigFactoryStub());
    $this->container->set('string_translation',
      $this->getStringTranslationStub());
    $this->container->set('messenger', $messenger);
    $this->container->set('media_keepeekdam.client', $keepeekdam_client_factory);
    $this->container->set('state', $this->state);

    \Drupal::setContainer($this->container);

    $this->keepeekDamConfig = $this->getMockedKeepeekdamConfig();
  }

}
