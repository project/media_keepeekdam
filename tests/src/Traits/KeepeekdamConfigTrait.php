<?php

namespace Drupal\Tests\media_keepeekdam\Traits;

/**
 * A shared mock config factory service.
 *
 * Provides configuration used by the different tests.
 */
trait KeepeekdamConfigTrait {

  /**
   * {@inheritdoc}
   */
  public function getConfigFactoryStub(array $configs = []) {
    return parent::getConfigFactoryStub([
      'media_keepeekdam.settings' => [
        'base_url' => 'https://keepeek.com/api',
        'username' => 'KPKusername',
        'password' => 'KPKpassword',
        'curl_timeout' => 20,
        'proxy_status' => 1,
        'proxy_url' => 'http://proxy.com:8080',
        'common_media_metadatas' => [
          'descriptor__keywords|field_description',
          'title|field_alt',
        ],
        'image_media_metadatas' => ['caption|field_title'],
        'video_media_metadatas' => ['caption|field_title'],
        'document_media_metadatas' => ['caption|field_title'],
        'image_styles_mapping' => ['style_1|kpk:style1|wvh'],
        'cdn_keepeek' => 'https://cdn.keepeek.com/',
        'cdn_keepeek_video_player' => 'https://cdn.keepeek.com/video',
        'results_per_page' => 20,
        'refresh_interval' => -1,
        'global_name' => 'Keepeek DAM',
        'id_published_status' => '1',
        'id_ready_for_publication_status' => '2',
      ],
    ] + $configs);
  }

}
