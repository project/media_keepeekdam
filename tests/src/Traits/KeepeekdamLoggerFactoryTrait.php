<?php

namespace Drupal\Tests\media_keepeekdam\Traits;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * A shared mock logger channel.
 */
trait KeepeekdamLoggerFactoryTrait {

  /**
   * Gets a stubbed out Logger factory for Media: Keepeek DAM test usage.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   A mock LoggerChannelFactoryInstance with a media_keepeekdam channel.
   */
  protected function getLoggerFactoryStub() {
    $logger_channel = $this->createMock(LoggerChannelInterface::class);

    $logger_factory = $this->createMock(LoggerChannelFactoryInterface::class);
    $logger_factory->method('get')
      ->with('media_keepeekdam')
      ->willReturn($logger_channel);

    return $logger_factory;
  }

}
