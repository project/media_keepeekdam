CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Media Keepeek DAM module provides Keepeek DAM integration for Media entity.
When a Keepeek DAM assets is added to a piece of content, this module will
create a media entity to be used into your contents.

This module uses Keepeek DAM's REST API to fetch assets and all the metadata.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/media_keepeekdam

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/media_keepeekdam

REQUIREMENTS
------------

This module requires the following core modules:

 * Media
 * Media Library
 * File
 * Image
 * Responsive Image

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/895232/ for further information.

CONFIGURATION
-------------

 * Once the module installed, go to /admin/config/media/keepeekdam and
 configure authentication credentials, search UI parameters,
 API mapping & cron configuration ...

 * Configure the user permissions in Administration » People » Permissions:
 Go to admin/people/permissions#module-media_keepeekdam

MAINTAINERS
-----------

Current maintainers:
 * Akram AMOURI (aamouri) - https://www.drupal.org/u/aamouri

This project has been sponsored by:
 * INETUM
 * AIRBUS
